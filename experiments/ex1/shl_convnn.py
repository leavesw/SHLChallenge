import os
import sys
import time
import logging
import argparse
import numpy as np
import tensorflow as tf
from datetime import datetime
from shlChallenge.data import ShlMag
from shlChallenge.ml import ShlConvNN
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import metrics


logger = logging.getLogger(os.path.basename(__file__))

default_data_folder_path = os.path.join(
    os.path.dirname(__file__),
    '../../shlData'
)

default_hdf5_file_path = os.path.join(
    default_data_folder_path,
    'shl_train_mag.hdf5'
)

default_log_dir = './log'
default_max_steps = 100000
default_batch_size = 128

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer('log_frequency', 10, "How often to log results to the console.")
tf.app.flags.DEFINE_boolean('log_device_placement', False, "Whether to log device placement.")
tf.app.flags.DEFINE_boolean('use_fp16', False, "Train the model use fp16.")


def train(data_hdf5, train_indices=None, validation_indices=None):
    """Train ShlConvNN for a number of steps.

    Global variables used in this function includes:
    - log_frequency: The number of steps between log prints.
    - batch_size: The batch size for training.
    - train_dir: The directory to store checkpoint data
    - max_steps: Max number of steps to run

    Args:
        data_hdf5: Loaded hdf5 dataset.
        train_indices: Indices for training data.
        validation_indices: Indices for validation.
    """
    assert(isinstance(data_hdf5, ShlMag))
    with tf.Graph().as_default():
        global_step = tf.train.get_or_create_global_step()

        # Get data and labels from ShlMagDataset
        tf_train_dataset = data_hdf5.to_tf_dataset(train_indices)
        batched_train_dataset = tf_train_dataset.batch(FLAGS.batch_size)
        # input = tf.placeholder(dtype=tf.float64,
        #                        shape=(FLAGS.batch_size, data_hdf5.feature_width, data_hdf5.num_channels),
        #                        name='input')
        # label = tf.placeholder(dtype=tf.int32,
        #                        shape=(FLAGS.batch_size,),
        #                        name='truth')
        with tf.device('/cpu:0'):
            data, label = batched_train_dataset.make_one_shot_iterator().get_next()

        # Create model
        model = ShlConvNN(num_classes=len(data_hdf5.ACTIVITIES),
                          num_channels=data_hdf5.num_channels,
                          conv_layers=[32, 32],
                          conv_filters=[10, 10],
                          local_layers=[200])

        # Build a graph that computes the logits predictions from the inference model
        logits = model.inference(input=data)

        # Calculate loss
        loss = model.loss(logits=logits, labels=label)

        # Build a graph that trains the model with one batch of examples and updates the model parameters.
        if train_indices is not None:
            epoch_size = len(train_indices)
        else:
            epoch_size = data_hdf5.data.shape[0]
        train_op = model.train(loss, global_step,
                               epoch_size=epoch_size,
                               num_epochs_per_decay=350,
                               init_learning_rate=0.000001,
                               learning_rate_decay_factor=0.1,
                               moving_average_decay=0.9999)

        # Build validation graph
        validation_y_true = data_hdf5.frame_label.value[validation_indices] - 1
        tf_validation_dataset = data_hdf5.to_tf_dataset(validation_indices)
        batched_validation_dataset = tf_validation_dataset.batch(FLAGS.batch_size)
        batched_validation_iterator = batched_validation_dataset.make_initializable_iterator()
        num_validation_batches = int(np.ceil(len(validation_indices) / FLAGS.batch_size))
        with tf.device('/cpu:0'):
            validation_data, validation_label = batched_validation_iterator.get_next()
        validation_logits = model.inference(input=validation_data)
        # validation_loss = model.loss(logits=validation_logits, labels=validation_label)
        validation_prediction = model.prediction(logits=validation_logits)

        class _LoggerHook(tf.train.SessionRunHook):
            """Logs loss and runtime."""

            def begin(self):
                self._step = -1
                self._start_time = time.time()

            def before_run(self, run_context):
                self._step += 1
                return tf.train.SessionRunArgs(loss)  # Asks for loss value.

            def after_run(self, run_context, run_values):
                if self._step % FLAGS.log_frequency == 0:
                    current_time = time.time()
                    duration = current_time - self._start_time
                    self._start_time = current_time

                    loss_value = run_values.results
                    examples_per_sec = FLAGS.log_frequency * FLAGS.batch_size / duration
                    sec_per_batch = float(duration / FLAGS.log_frequency)

                    format_str = ('%s: step %d, loss = %.2f (%.1f examples/sec; %.3f '
                                  'sec/batch)')
                    print(format_str % (datetime.now(), self._step, loss_value,
                                        examples_per_sec, sec_per_batch))

        gpu_config = tf.ConfigProto(
            log_device_placement=FLAGS.log_device_placement,
        )
        gpu_config.gpu_options.per_process_gpu_memory_fraction = 0.7
        i = -1
        with tf.train.MonitoredTrainingSession(
                checkpoint_dir=FLAGS.train_dir,
                hooks=[tf.train.StopAtStepHook(last_step=FLAGS.max_steps),
                       tf.train.NanTensorHook(loss),
                       _LoggerHook()],
                config=gpu_config) as mon_sess:
            mon_sess.run(batched_validation_iterator.initializer)
            while not mon_sess.should_stop():
                mon_sess.run(train_op)
                if i % FLAGS.log_frequency == 0:
                    mon_sess.run(batched_validation_iterator.initializer)
                    validation_y_pred = np.zeros((num_validation_batches * FLAGS.batch_size))
                    for j in range(num_validation_batches):
                        validation_y_pred[j*FLAGS.batch_size:(j+1)*FLAGS.batch_size] = mon_sess.run(
                            validation_prediction
                        )
                    validation_y_pred = validation_y_pred[0:len(validation_indices)]
                    print('Validation Performance:')
                    print('Accuracy: %.4f' % metrics.accuracy_score(validation_y_true, validation_y_pred))
                    print('Precision: %.4f' % metrics.precision_score(validation_y_true, validation_y_pred, average='macro'))
                    print('Recall: %.4f' % metrics.recall_score(validation_y_true, validation_y_pred, average='macro'))
                    print('F-1: %.4f' % metrics.f1_score(validation_y_true, validation_y_pred, average='macro'))
                i += 1


def parse_args():
    parser = argparse.ArgumentParser(
        description="Show animation of SHL dataset."
    )
    parser.add_argument('-d', '--hdf5', type=str,
                        default=default_hdf5_file_path,
                        help="SHL dataset in HDF5 format")
    parser.add_argument('-b', '--batch-size', type=int,
                        default=default_batch_size,
                        help='Batch size for training.')
    parser.add_argument('-s', '--max-steps', type=int,
                        default=default_max_steps,
                        help='Max number of iterations.')
    parser.add_argument('--log', type=str, default=default_log_dir,
                        help='Log directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increase output verbosity.')
    return parser.parse_args()


def check_args(args):
    if not os.path.isfile(args.hdf5):
        logger.error('Cannot find hdf5 file at %s.' %
                     args.hdf5)
        sys.exit(1)
    tf.app.flags.DEFINE_integer('batch_size', args.batch_size, "Batch size.")
    tf.app.flags.DEFINE_integer('max_steps', args.max_steps, "Number of batches to run.")
    os.makedirs(args.log, exist_ok=True)
    tf.app.flags.DEFINE_string('train_dir', args.log, "Directory where to write event logs and checkpoints.")
    if tf.gfile.Exists(FLAGS.train_dir):
        tf.gfile.DeleteRecursively(FLAGS.train_dir)
    tf.gfile.MakeDirs(FLAGS.train_dir)


if __name__ == '__main__':
    args = parse_args()
    check_args(args=args)
    shlmag = ShlMag(args.hdf5)
    labels = shlmag.frame_label.value
    sss = StratifiedShuffleSplit(n_splits=1, random_state=0, train_size=0.8)
    train_indices, test_indices = list(sss.split(X=labels, y=labels))[0]
    train(shlmag, train_indices=train_indices, validation_indices=test_indices)
