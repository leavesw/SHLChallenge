"""This script translates txt based SHL dataset into HDF5 format for faster
query and disk I/O.
"""

import os
import sys
import logging
import argparse
from datetime import datetime
from shlChallenge.data import ShlHdf5

logger = logging.getLogger(os.path.basename(__file__))

default_data_folder_path = os.path.join(
    os.path.dirname(__file__),
    '../shlData'
)
default_data_path = os.path.join(
    default_data_folder_path,
    'train'
)
default_data_label_path = os.path.join(
    default_data_path,
    'Label.txt'
)
default_data_order_path = os.path.join(
    default_data_path,
    'train_order.txt'
)
default_hdf5_file_path = os.path.join(
    default_data_folder_path, 'shl_train.hdf5'
)


def parse_args():
    parser = argparse.ArgumentParser(
        description="Translate SHL dataset into HDF5 format."
    )
    parser.add_argument('-o', '--output', type=str,
                        default=default_hdf5_file_path,
                        help="SHL HDF5 filename")
    parser.add_argument('-d', '--data', type=str,
                        default=default_data_path,
                        help='SHL Dataset folder containing txt files.')
    parser.add_argument('-l', '--label', type=str,
                        default=None,
                        help='SHL Dataset label file (.txt).')
    parser.add_argument('-r', '--order', type=str,
                        default=None,
                        help='SHL Dataset order file (.txt)')
    parser.add_argument('--log', type=str, default=None,
                        help='Log directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Increase output verbosity.')
    return parser.parse_args()


def check_args(args):
    if not os.path.isdir(args.data):
        logger.error('Cannot find data at %s.' %
                     args.data)
        sys.exit(1)
    if args.data == default_data_path:
        if args.label is None:
            args.label = default_data_label_path
        if args.order is None:
            args.order = default_data_order_path
    if args.label is not None and not os.path.isfile(args.label):
        logger.error('Cannot find data label at %s' %
                     args.label)
        sys.exit(1)
    if args.order is not None and not os.path.isfile(args.order):
        logger.error('Cannot find data order file at %s' %
                     args.order)
        sys.exit(1)


def config_logger(log_path=None, verbose=False):
    """Configure logging options.
    """
    if verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    log_handlers = [logging.StreamHandler()]
    if log_path is not None:
        os.makedirs(log_path, exist_ok=True)
        log_filename = '%s_%s.log' % (
            os.path.basename(__file__),
            datetime.now().strftime('%y%m%dT%H%M%S')
        )
        log_handlers.append(
            logging.FileHandler(
                os.path.join(log_path, log_filename)
            )
        )
    logging.basicConfig(
        level=log_level,
        format='[%(asctime)s] %(name)s:%(levelname)s:%(message)s',
        handlers=log_handlers
    )


if __name__ == "__main__":
    args = parse_args()
    check_args(args=args)
    config_logger(log_path=args.log, verbose=args.verbose)
    dataset = ShlHdf5.from_folder(
        output_file=args.output,
        data_path=args.data,
        label_path=args.label,
        order_path=args.order
    )
