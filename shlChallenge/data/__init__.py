from ._hdf import ShlHdf5
from ._dwt import ShlDwt
from ._mag import ShlMag
