import numpy as np
import tensorflow as tf
from ._tf_helper import activation_summary
from ._tf_helper import add_loss_summaries
from ._tf_helper import variable_on_cpu
from ._tf_helper import variable_with_weight_decay


FLAGS = tf.app.flags.FLAGS


class ShlMLP:
    """Multi-layer Perceptron for activity recognition with SHL Dataset.

    Construct a convolutional neural network with required parameters.

    Args:
        num_classes: Number of target classes.
        local_layers: List of integers, specifying the hidden neurons of fully-connected hidden layer.
    """
    def __init__(self, num_classes,
                 local_layers=[192, 192]):
        self.input = input
        self.num_classes = num_classes
        self.local_layers = local_layers
        pass

    def inference(self, input):
        """Build multi-layer perceptron model.

        Args:
            input: Input tensor, of shape `[batch_size, frame_length, num_channels]`.

        Returns:
            Logits
        """
        # Kernels for each convolutional layer
        layer_input = input
        local_layers = self.local_layers
        num_classes = self.num_classes

        input_size = layer_input.get_shape()[1].value
        for i, num_units in enumerate(local_layers):
            local_layer_name = 'local%d' % i
            with tf.variable_scope(local_layer_name, reuse=tf.AUTO_REUSE) as scope:
                weights = variable_with_weight_decay(
                    name='weights',
                    shape=[input_size, num_units],
                    stddev=0.04,
                    wd=0.004
                )
                bias = variable_on_cpu(
                    name='biases',
                    shape=[num_units],
                    initializer=tf.constant_initializer(0.)
                )
                layer_input = tf.nn.relu(tf.matmul(layer_input, weights) + bias, name=scope.name)
                input_size = num_units

        # linear layer(WX + b),
        # We don't apply softmax here because
        # tf.nn.sparse_softmax_cross_entropy_with_logits accepts the unscaled logits
        # and performs the softmax internally for efficiency.
        with tf.variable_scope('softmax_linear', reuse=tf.AUTO_REUSE) as scope:
            weights = variable_with_weight_decay('weights', [input_size, num_classes],
                                                 stddev=1 / 192.0, wd=None)
            biases = variable_on_cpu('biases', [num_classes],
                                     tf.constant_initializer(0.0))
            softmax_linear = tf.add(tf.matmul(layer_input, weights), biases, name=scope.name)
            activation_summary(softmax_linear)
        return softmax_linear

    def loss(self, logits, labels):
        """Add L2Loss to all the trainable variables.

        Add summary for "Loss" and "Loss/avg"

        Args:
            logits: Logits from inference().
            labels: Labels from distorted_inputs or inputs(). 1-D tensor
                    of shape [batch_size]
        Returns:
            Loss tensor of type float.
        """
        # Calculate the average cross entropy loss across the batch
        labels = tf.cast(labels, tf.int64)
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=labels,
            logits=logits,
            name='cross_entropy_per_example'
        )
        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
        tf.add_to_collection('losses', cross_entropy_mean)

        # The total loss is defined as the cross entropy loss plus all of the weight decay terms (L2)
        return tf.add_n(tf.get_collection('losses'), name='total_loss')

    def prediction(self, logits):
        return tf.argmax(logits, axis=1)

    def prediction_prob(self, logits):
        return tf.nn.softmax(logits=logits)

    def train(self, total_loss, global_step,
              epoch_size, num_epochs_per_decay,
              init_learning_rate, learning_rate_decay_factor,
              moving_average_decay):
        """Train ShlConvNN model

        Returns:
             train_op: The training operator
        """
        # Variable that affect learning rate.
        num_batches_per_epoch = epoch_size / FLAGS.batch_size
        decay_steps = int(num_batches_per_epoch * num_epochs_per_decay)

        # Decay the learning rate exponentially based on the number of steps
        lr = tf.train.exponential_decay(
            init_learning_rate,
            global_step,
            decay_steps,
            learning_rate_decay_factor,
            staircase=True
        )
        tf.summary.scalar('learning_rate', lr)

        # Generate moving average of loss
        loss_averages_op = add_loss_summaries(total_loss=total_loss)

        # Compute Gradients
        with tf.control_dependencies([loss_averages_op]):
            opt = tf.train.GradientDescentOptimizer(learning_rate=lr)
            grads = opt.compute_gradients(total_loss)

        # Apply gradients
        apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)

        # Add histograms for trainable variables
        for var in tf.trainable_variables():
            tf.summary.histogram(var.op.name, var)

        # Add histograms for gradients
        for grad, var in grads:
            if grad is not None:
                tf.summary.histogram(var.op.name + '/gradients', grad)

        # Track the moving averages of all trainable variables.
        variable_averages = tf.train.ExponentialMovingAverage(
            decay=moving_average_decay,
            num_updates=global_step
        )

        with tf.control_dependencies([apply_gradient_op]):
            variables_averages_op = variable_averages.apply(tf.trainable_variables())

        return variables_averages_op
